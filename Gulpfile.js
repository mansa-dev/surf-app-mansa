'use strict';

// Basic Gulp File
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    notify = require("gulp-notify"),
    bower = require('gulp-bower'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    templateCache = require('gulp-angular-templatecache');


var config = {
    sassPath: './client',
    bowerDir: './bower_components',
    appPath: './client'
};

gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(config.bowerDir));
});

gulp.task('icons', function() {
    return gulp.src([
        config.bowerDir + '/fontawesome/fonts/**.*',
        config.bowerDir + '/bootstrap-sass-official/assets/fonts/bootstrap/**.*'
    ])
        .pipe(gulp.dest('./public/fonts'));
});

gulp.task('css', function() {
    return gulp.src(config.sassPath + '/sass/style.scss')
        .pipe(sass({
            style: 'compressed',
            loadPath: [
                config.sassPath,
                config.bowerDir + '/bootstrap-sass-official/assets/stylesheets',
                config.bowerDir + '/fontawesome/scss',
                config.bowerDir + '/angular-slider'
            ]
        })
        .on("error", notify.onError(function (error) {
                return "Error: " + error.message;
            })))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('js', function(){
    return gulp.src([
        config.appPath + '/js/features/**/*.js',
        config.appPath + '/js/services/**/*.js',
        config.appPath + '/js/app.js'
    ])
    .pipe(uglify())
    .pipe(concat('core.js'))
    .pipe(gulp.dest('./public/js'));
});

gulp.task('vendor', function(){
    return gulp.src([
        config.bowerDir + '/jquery/dist/jquery.min.js',
        config.bowerDir + '/bootstrap-sass-official/assets/javacripts/bootstrap.js',
        config.bowerDir + '/lodash/lodash.min.js',
        config.bowerDir + '/angular/angular.min.js',
        config.bowerDir + '/angular-route/angular-route.min.js',
        config.bowerDir + '/angular-animate/angular-animate.min.js',
        config.bowerDir + '/angular-bootstrap/ui-bootstrap-tpls.min.js',
        config.bowerDir + '/angular-slider/slider.js'
    ])
    .pipe(uglify())
    .pipe(concat('vendor.js'))
    .pipe(gulp.dest('./public/js'));
});

gulp.task('templates', function(){
    return gulp.src(config.appPath + '/**/*.html')
        .pipe(templateCache({
            standalone: true
        }))
        .pipe(gulp.dest('./public/js'));
});

gulp.task('watch', function() {
    gulp.watch(config.sassPath + '/**/*.scss', ['css']);
});

gulp.task('default', ['bower', 'icons', 'css', 'vendor', 'js', 'templates']);
gulp.task('dev', [ 'css', 'js', 'templates']);
