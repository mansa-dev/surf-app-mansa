(function(angular, _){
    'use strict';
    angular.module('surfapp.services.data', [])
    .service('DataService', ['$http', '$q', 'env', function($http, $q, env){

        var cachedDataSet = null; //save the data to Application state

        return {
            /**
             * Get data key from the cached / live set
             */
            getDataByKey: function(key){
                var deferred = $q.defer();
                this.getCachedData()
                    .then(function(data){
                        deferred.resolve(data[key]);
                    })
                    .catch(deferred.reject);
                return deferred.promise;
            },
            /**
             * Get the data from live or live api, Cache it in application state
             */
            getCachedData: function(){
                var deferred = $q.defer();
                if(cachedDataSet){
                    deferred.resolve(cachedDataSet);
                } else {
                    this.getData().then(function(data){
                        cachedDataSet = data;
                        deferred.resolve(data);
                    }).catch(deferred.reject);
                }
                return deferred.promise;
            },
            /**
             * Get the data from live service
             */
            getData: function(){
                var deferred = $q.defer();
                $http.get(env.apiUrl + '/data/volume.json').then(function(resp){
                    deferred.resolve(resp.data[0]);
                }).catch(deferred.reject);
                return deferred.promise;
            },
            /**
             * Get the weight from dataset
             */
            getWeights: function(){
                return this.getDataByKey('weight');
            },
            /**
             * Get the ages set from dataset
             */
            getAges: function(){
                return this.getDataByKey('age');
            },
            /**
             * Get the abilities from dataset
             */
            getAbilities: function(){
                return this.getDataByKey('ability');
            },
            /**
             * Get the fitness from dataset
             */
            getFitness: function(){
                return this.getDataByKey('fitness');
            },
            /**
             * Get the volumes from dataset
             */
            getVolumes: function(){
                return this.getDataByKey('volumes');
            },
            /**
             * Get the genders
             */
            getGenders: function(){
                return ['Male', 'Female'];
            }
        };
    }]);
}(angular, _));
