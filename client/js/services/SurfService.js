(function(angular){
    'use strict';
    angular.module('surfapp.services.surf', [])
    .service('SurfService', [
        '$http',
        '$q',
        'env',
        function($http, $q, env){

        //We can use Promise based API structure when live Api is used
        //For now use the presupplied data

        /** Generate randome string like 1-2, 4/5
         *
         * @param joinStr , String, char to join with , default "/"
         *
         **/
         var genRandomString = function(joinStr){
             joinStr = joinStr || "/";

             var lowerLimit = (parseInt(Math.random() * 10) + 2);
             var seed = (parseInt(((Math.random() * 10) + 2) / 2));
             var upperLimit = lowerLimit + seed;

             return lowerLimit + joinStr + upperLimit;
         };

        /** Generate a random board
         *
         * @param name , String , name of board
         *
         **/
        var genRandomBoard = function(name){
            return {
                'name': name,
                'wave': genRandomString('-'),
                'speed': genRandomString('/'),
                'paddle': genRandomString('/'),
                'manovourabilty': genRandomString('/'),
                'price': parseInt((Math.random() * 1000) + (Math.random() * 100))
            };
        };

        return {
            getRandomBoards: function(count){
                count = count || 10;
                var data = [];
                for(var i = 0; i <= count; i++){
                    data.push(genRandomBoard('Board No. ' + parseInt(Math.random() * 10)));
                }
                return data;
            },
            getAllSurfBoards: function(){
                var deferred = $q.defer();
                $http.get(env.apiUrl + '/data/boards.json')
                .then(function(resp){
                    deferred.resolve(resp.data.shapes);
                })
                .catch(deferred.reject);
                return deferred.promise;
            }
        };

    }]);

}(angular));
