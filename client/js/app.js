(function(angular){
'use strict';
angular.module('surfapp', [
    'ngRoute',
    'ngAnimate',
    'ui.bootstrap',
    'surfapp.features.surf',
    'surfapp.services.data',
    'templates'
])
.constant('env', {
    apiUrl: 'https://surf-app-mansa.herokuapp.com'
})
.config(['$routeProvider', function($routeProvider){
    $routeProvider.when('/select', {
        controller: 'surfapp.controllers.SurfController',
        templateUrl: 'js/features/surf/surf.html',
        resolve: {
            data: ['DataService', function(DataService){
                return DataService.getCachedData();
            }]
        }
    });
    $routeProvider.when('/', {
        controller: ['$scope', function($scope){
            $scope.titlePage = "Welcome";
        }],
        template: '<h2 class="text-center">{{titlePage}}</h2>'
    });
}])
.run(['$location', function($location){
    $location.path('/select');
}])
.controller('surfapp.controllers.AppController', ['$scope', function($scope){
    $scope.title = "Main";
}]);

}(angular));
