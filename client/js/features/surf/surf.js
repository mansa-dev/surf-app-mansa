(function(angular, _){
    'use strict';
    angular.module('surfapp.features.surf', [
        'ui.slider',
        'surfapp.services.surf',
        'surfapp.services.data',
        'ngAnimate'
    ])
    .filter('boardsFilter', [function(){
        return function(data, minVol, maxVol){
            return _.filter(data, function(board){
                var volumes = _.pluck(board.shapecustomisations.lengths, 'volume');
                var found = false;
                volumes.forEach(function(v){
                    if(_.inRange(v, minVol - 1, maxVol + 1)){
                        found = true;
                    }
                });
                return found;
            });
        };
    }])
    .controller('surfapp.controllers.SurfController', [
        '$scope',
        '$q',
        '$filter',
        'SurfService',
        'DataService',
        function($scope, $q, $filter, SurfService, DataService){

            $scope.boards = [];
            $scope.preset = null; //actual prset returned by api
            $scope.sliderPreset = null; //parsed preset so slider can work with them

            SurfService
            .getAllSurfBoards()
            .then(function(data){
                $scope.boards = data;
            }).catch(function(e){
                console.error("Error Getting Boards " + e.message);
            });

            //get all required preset data
            $q.all([
                DataService.getGenders(),
                DataService.getAbilities(),
                DataService.getFitness(),
                DataService.getWeights(),
                DataService.getVolumes(),
                DataService.getAges()
            ])
            //parse and prepare them
            .then(function(data){
                $scope.preset = {
                    gender: data[0],
                    abilities: data[1],
                    fitness: data[2],
                    weight: data[3],
                    volumes: data[4],
                    ages: data[5]
                };
                $scope.sliderPreset = {
                    gender: data[0],
                    abilities: _.pluck(data[1], 'level'),
                    fitness: _.pluck(data[2], 'level'),
                    weight: _.pluck(data[3], 'id')
                };
            }).catch(function(e){
                console.error("Error Getting Presets " + e.message);
            });

            //track selected data by user
            $scope.surfData = {
                gender: 0,
                weight: 0,
                ability: 0,
                age: 10,
                fitness: 0
            };

            /**
             * Get the ideal volume for the current settings
             *
             * @param ability    , String,  The ability's level id selected
             * @param weight     , Integer, The actual weight selected
             * @param fitnessMul , Float,   The mutipler selected based upon the fitness selected
             *
             * @return String, for ideal volume like 35-45
             */
            $scope.getAllowedVolume = function(ability, weight, fitnessMul){
                $scope.loweLevel = parseInt(_.result(_.findWhere($scope.preset.volumes, { 'ability': ability, 'weightid': weight }), 'volume'));
                $scope.higherLevel = parseInt($scope.loweLevel * fitnessMul * fetchAgeMutiplier());
                return $scope.loweLevel + " - " + $scope.higherLevel;
            };

            /**
             * Fetch the age mutipler for the selected age
             *
             * @return Integer, The age mutipler which is selected
             */
            var fetchAgeMutiplier = function(){
                var preset = 1;
                $scope.preset.ages.forEach(function(agePreset){
                    if(inRange(agePreset.years, $scope.surfData.age)){
                        preset = agePreset.multiplier;
                    }
                });
                return preset;
            };

            /**
             * Get a realrange checked for a number
             *
             * @param realRange, String , The range of range world numbers like 0-30 , 81+
             * @param number   , Integer, The number to check to
             *
             * @return Boolean, True if number is in range
             */
            var inRange = function(realRange, number){
                    if(realRange.indexOf('-') > 0){
                        var tmp1 = realRange.split('-');
                        return _.inRange(number, parseInt(tmp1[0] - 1), parseInt(tmp1[1] + 1));
                    } else if(realRange.indexOf('+') > 0){
                        var tmp2 = realRange.split('+');
                        return number >= parseInt(tmp2[0]);
                    } else {
                        return false;
                    }
            };

            $scope.$watch('surfData.age', function(age){
                var classPrepare = 'slider-age character-';

                if(age <= 30)
                    classPrepare += "young";
                else if(age <= 40)
                    classPrepare += "ymid";
                else if(age <= 50)
                    classPrepare += "slower";
                else if(age <= 60)
                    classPrepare += "middle";
                else if(age > 60)
                    classPrepare += "old";
                else
                    classPrepare += "young";

                $scope.ageClass = classPrepare;
            });

            $scope.$watch('surfData.weight', function(weightId){
                var classPrepare = 'slider-weight character-';
                var weight = $scope.preset.weight[weightId].kg

                if(weight <= 40)
                    classPrepare += "thin";
                else if(weight <= 60)
                    classPrepare += "thinish";
                else if(weight <= 80)
                    classPrepare += "mid";
                else if(weight > 80)
                    classPrepare += "fat";
                else
                    classPrepare += "thin";

                $scope.weightClass = classPrepare;
            });

            $scope.$watch('surfData.ability', function(ability){
                var classPrepare = 'slider-ability character-';
                var abilityLevel = $scope.preset.abilities[ability].level
                $scope.abilityClass = classPrepare + abilityLevel;

            });

        }]);

}(angular, _));
